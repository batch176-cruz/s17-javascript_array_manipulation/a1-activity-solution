let students  = [];

function addStudent(name) {
	students.push(name);
	console.log(name + " was added to the student's list.")
};

function countStudents() {
	console.log("There are a total of " + students.length + " students enrolled.");
};

function printStudents() {
	students.sort();
	students.forEach(function(print) {
		console.log(print);
	});
};

let findStud = [];

function findStudent(input) {
	findStud = students.filter(function(name) {
		return name.toLowerCase().includes(input);
	});

	if (findStud.length == 0) {
		console.log(input + " is not an enrollee.");
	} else if (findStud.length == 1) {
		console.log(findStud + " is an enrollee.");
	} else {
		console.log(findStud + " are enrollees.");
	}
};

let studentSection = [];

function addSection(input) {
	studentSection = students.map(function(name) {
		return name + " - section " + input;
	});
	console.log(studentSection);
};

let tempName;
let firstLetter;
let index;

function removeStudent(input) {
	firstLetter = input[0].toUpperCase();
	tempName = input.slice(1);
	index = students.indexOf(firstLetter + tempName);
	
	if (index == 0) {
		students.shift();
	} else {
		students.splice(index, 1);
	}

	console.log(firstLetter + tempName + " was removed from the student's list.")
};